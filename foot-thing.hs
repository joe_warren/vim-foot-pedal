{-# LANGUAGE TypeSynonymInstances #-}
{-# LANGUAGE FlexibleInstances #-}
module Object where

import qualified Csg
import qualified Csg.STL
import Data.List
import Data.Serialize
import Data.Semigroup
import Data.Vec3 as V3
import qualified Data.Text.IO as T

tombstone :: Double -> Double -> Double -> Csg.BspTree
tombstone r h d = Csg.unionConcat [
        Csg.scale (r, r, d) $ Csg.unitCylinder 16,
        Csg.scale (r * 2, h, d) $ Csg.translate (0, -0.5, 0) Csg.unitCube
    ]

hinge :: Double -> Double -> Double -> Csg.BspTree
hinge r m l = Csg.rotate(0, 0, 1) (pi/2) $ Csg.rotate (1, 0, 0) (pi/2) $ 
                (tombstone (r + m) (r + m) l) `Csg.subtract` (
                    Csg.scale (r, r, l*2) $ Csg.unitCylinder 16)
 

base :: Csg.BspTree
base = Csg.unionConcat [
     tombstone 40 80 1,
     Csg.translate (0, 4.5-80, 4.5) $ hinge 2.5 2 76, 
      Csg.subtract (
         Csg.intersection
            (Csg.translate (0, 0, 0.01 + 25/2) $ (
                tombstone 38 70 25 `Csg.subtract`
                tombstone 37 80 30
            ))
            (Csg.translate (0, -70, 0) $ 
                Csg.rotate (1,0,0) (pi/8) $ 
                    Csg.uniformScale 1000 $
                        Csg.translate (0, 0, -0.5) Csg.unitCube)
        ) (
        Csg.translate (0, 0, 8/2+0.01) $ Csg.rotate (1, 0, 0) (pi/2) $ 
            tombstone 8 8 200
        ), 
    Csg.translate (0, -3, 0) $ Csg.subtract (Csg.unionConcat [ 
            Csg.translate (i * 3.5, 0, 25/2) $ 
                Csg.scale (2, 13, 25) Csg.unitCube 

        | i <- [-1, 1]]) 
        (Csg.unionConcat [
            Csg.translate (0, i *3, 25 - 5) $ 
                Csg.rotate (0, 1, 0) (pi/2) $
                    Csg.scale (1, 1, 30) $
                        Csg.unitCylinder 8
        | i <- [-1, 1]]), 
    Csg.translate (0, 13/2 + 32/2 -3 , 1 + 3/2) $ 
        Csg.subtract (
            Csg.scale (18.2+2, 30.5+2, 3) Csg.unitCube
          ) (
            Csg.scale (18.2, 30.5, 6) Csg.unitCube
          ),
    Csg.unionConcat  [
        Csg.translate (i*20, 10, 25/2) $ 
            Csg.subtract (
                Csg.scale (12, 12, 25) Csg.unitCube 
                ) ( Csg.scale (9, 9, 30) Csg.unitCube)
        | i <- [-1, 1]]
                
  ]
topCast :: Double -> Csg.BspTree
topCast r = Csg.intersection
        (Csg.translate (0, 0, 26/2 + 1) $ tombstone r (80 - 4.5) 26
    )(
        (Csg.translate (0, 4.5 - 80 , 4.5) $ 
          Csg.rotate (1,0,0) (pi/8) $ 
            Csg.uniformScale 1000 $
                Csg.translate (0, 0, -0.5) Csg.unitCube)
    )

top = Csg.subtract (
          Csg.union (           
              Csg.subtract 
                (topCast 41)
                (Csg.translate (0, 0, -2) (topCast 40))
            ) (
              Csg.translate (0, 4.5-80, 4.5) $ 
                Csg.rotate (0, 1, 0) (pi/2) $
                  Csg.scale (2.5 + 2, 2.5 +2 , 82) $ Csg.unitCylinder 16
            )
        ) (
          Csg.unionConcat [
           Csg.translate (0, 4.5-80, 4.5) $ Csg.scale (78, 10, 10) Csg.unitCube,
           Csg.translate (0, 4.5-80, 4.5) $ 
            Csg.rotate (0, 1, 0) (pi/2) $
                Csg.scale (2.5, 2.5, 100) $ Csg.unitCylinder 16, 
           Csg.translate (0, 0, 1) $    
             Csg.uniformScale 1000 $ Csg.translate (0, 0, -0.5) $ Csg.unitCube,

           Csg.translate (0, 30, 8/2+0.01) $ Csg.rotate (1, 0, 0) (pi/2) $ 
            tombstone 10 10 60
          ]
        ) 

spring :: Csg.BspTree
spring = Csg.subtract (Csg.scale (8, 8, 31) $ Csg.translate (0, 0, 0.5) Csg.unitCube) $ 
            Csg.unionConcat [
              Csg.translate (if (even i) then -1 else 1, 0, 1.5 + (fromIntegral i) * 2) $
                Csg.scale (8, 10, 1) Csg.unitCube
            | i <- [0..30]]


path1= "foot-thing.stl"
path2= "foot-thing-top.stl"
path3= "foot-thing-spring.stl"
main :: IO ()
main = do
    T.writeFile path1 $ Csg.STL.toSTL base
    T.writeFile path2 $ Csg.STL.toSTL top
    T.writeFile path3 $ Csg.STL.toSTL spring

