#include <Bounce.h>
int count = 0;
Bounce button1 = Bounce(10, 10); 
void setup() { } // no setup needed
void loop() {
  // Update all the button objects.
  button1.update();
  if (button1.fallingEdge()) {
    Keyboard.press(KEY_A);
    Keyboard.release(KEY_A);
  }
  if (button1.risingEdge()) {
    Keyboard.press(KEY_ESC);
    Keyboard.release(KEY_ESC);
  }
}
